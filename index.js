// 3.
fetch('https://jsonplaceholder.typicode.com/todos')
.then(res => res.json()) 
.then(response => console.log(response))


// 4.
fetch('https://jsonplaceholder.typicode.com/todos')
.then((res) => res.json())
.then((json) => {

	let list = json.map((todo => {
		return todo.title;
	}))

	console.log(list);

});


// 5.
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(res => res.json()) 
.then(response => console.log(response))


// 6.
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then(res => console.log("The item " + res.title + " on the list has a status of " + res.completed))


// 7.
fetch('https://jsonplaceholder.typicode.com/todos', 
	{
		method: "POST",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			completed: false,
			id: 201,
			title: "Created To Do List Item",
			userId:1
		})
	})
	.then(res => res.json())
	.then(res => console.log(res))


// 8-9. 

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body:JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		status: "Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))


// 10-11.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		status: "Complete",
		dateCompleted: "08/02/2023"
	})
})
.then(response => response.json())
.then(json => console.log(json))


// 12.
fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "DELETE"
	}	
).then(res => res.json())
.then(res => console.log(res))

